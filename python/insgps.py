'''
Wraper module for OpenPilot insgps code from matlab/ins
'''

import ctypes
from functools import partial
from ctypes import *
import os
import platform
import math

DIR = os.path.dirname(os.path.abspath(__file__))
EXTENSION = ".so" if "windows" not in platform.platform().lower() else ".dll"
SO = "libinsgps%s" % EXTENSION
try:
    dll = ctypes.PyDLL(os.path.join(DIR, SO))
except:
    dll = ctypes.PyDLL(os.path.join(DIR, "..", "result", SO))

#__all__ = [ "POS_SENSORS", "HORIZ_SENSORS", "VERT_SENSORS", "MAG_SENSORS",
#            "BARO_SENSORS", "FULL_SENSORS",
#            "INSGpsInit", "INSStatePrediction", "INSCovariancePrediction"]

# constants
POS_SENSORS   = 0x007
HORIZ_SENSORS = 0x018
VERT_SENSORS  = 0x020
MAG_SENSORS   = 0x1C0
BARO_SENSORS  = 0x200
FULL_SENSORS  = 0x3FF
NUMX = 13 # number of states
NUMW = 9  # number of plant noise inputs
NUMV = 10 # number of measurements
NUMU = 6  # number of determinitic inputs

# internal data type
float_mat_F = c_float * NUMX * NUMX
float_mat_G = c_float * NUMX * NUMW
float_mat_H = c_float * NUMV * NUMX
float_mat_K = c_float * NUMX * NUMV
float_vector3  = c_float * 3
float_vector2  = c_float * 2
float_vector4  = c_float * 4
float_vector_X = c_float * NUMX
float_vector_Q = c_float * NUMW
float_vector_R = c_float * NUMV

class EKFData(Structure):
    _fields_ = [
        # linearized system matrices
        ('F', float_mat_F),
        ('G', float_mat_G),
        ('H', float_mat_H),

        # local magnetic unit vector in  NED frame
        ('Be', float_vector3),

        # covariance matrix and state vector
        ('P', float_mat_F),
        ('X', float_vector_X),

        # input noise and measurement noise variances
        ('Q', float_vector_Q),
        ('R', float_vector_R),
    ]



def RollPitchYaw2Quaternions(roll, pitch, yaw):
    q0 = 0.0
    q1 = 0.0
    q2 = 0.0
    q3 = 0.0

    phi = roll/2
    theta = pitch/2
    psi = yaw/2

    cphi = math.cos(phi)
    sphi = math.sin(phi)
    ctheta = math.cos(theta)
    stheta = math.sin(theta)
    cpsi = math.cos(psi)
    spsi = math.sin(psi)

    q0 = cphi * ctheta * cpsi + sphi * stheta * spsi
    q1 = sphi * ctheta * cpsi - cphi * stheta * sphi
    q2 = cphi * stheta * cpsi + sphi * ctheta * sphi
    q3 = cphi * ctheta * spsi - sphi * stheta * cpsi

    # q0 always positive for uniqueness
    if q0 < 0:
        q0 = -q0
        q1 = -q1
        q2 = -q2
        q3 = -q3

    return [q0, q1, q2, q3]

def Quaternion2RollPitchYaw(quat):
    q0 = quat[0]
    q1 = quat[1]
    q2 = quat[2]
    q3 = quat[3]

    q0s = q0 ** 2.
    q1s = q1 ** 2.
    q2s = q2 ** 2.
    q3s = q3 ** 2.


    R13 = 2. * ( q1 * q3 - q0 * q2 )
    R11 = q0s + q1s - q2s - q3s
    R12 = 2. * ( q1 * q2 + q0 * q3 )
    R23 = 2. * ( q2 * q3 + q0 * q1 )
    R33 = q0s - q1s - q2s + q3s

    pitch = math.asin ( -R13 )
    yaw   = math.atan2( R12, R11 )
    roll  = math.atan2( R23, R33 )

    return [ math.degrees(x) for x in [ roll, pitch, yaw ] ]


class NavStruct(Structure):
    _fields_ = [ ( "pos"       , c_float * 3 ),
                 ( "vel"       , c_float * 3 ),
                 ( "quat"      , c_float * 4 ),
                 ( "gyro_bias" , c_float * 3 ),
                 ( "accel_bias", c_float * 3 )
    ]


    def __str__(self):
        out = "%010.6f %010.6f %010.6f" % tuple(self.pos) + " "
        out+= "%010.5f %010.5f %010.5f" % tuple(self.vel) + " "
        out+= "%05.3f %05.3f %05.3f"    % tuple(self.getRollPitchYaw()) + " "
        out+= "%05.3f %05.3f %05.3f"    % tuple(self.gyro_bias) + " "
        out+= "%05.3f %05.3f %05.3f"    % tuple(self.accel_bias) + " "
        return out

    def getRollPitchYaw(self):
        return Quaternion2RollPitchYaw(self.quat)

# global variables
Nav = NavStruct.in_dll(dll, "Nav") # state matrix in navigation form
ekf = EKFData.in_dll(dll, "ekf")

# internal helpers
def getFunction(name, argtypes=None, restypes=None):
    out = getattr(dll, name)
    out.argtypes = argtypes
    out.restype = restypes
    return out

def convertFunction1(func, al, bl, c):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    return func(a, b, c)

def convertFunction2(func, al):
    a = listToCFloatArray3(al)
    return func(a)

def convertFunction3(func, al, bl):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    return func(a, b)

def convertFunction4(func, al, bl, cl, d):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    c = listToCFloatArray3(cl)
    return func(a, b, c, d)

def convertFunction5(func, al, bl, cl):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    c = listToCFloatArray2(cl)
    return func(a, b, c)

def convertFunction6(func, al, b):
    a = listToCFloatArray3(al)
    return func(a, b)

def convertFunction7(func, al):
    a = listToCFloatArrayX(al)
    return func(a)

def convertFunction8(func, al, bl, cl, dl, el):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    c = listToCFloatArray4(cl)
    d = listToCFloatArray3(dl)
    e = listToCFloatArray3(el)
    return func(a, b, c, d, e)

def convertFunction9(func, al, bl, cl,d, e):
    a = listToCFloatArray3(al)
    b = listToCFloatArray3(bl)
    c = listToCFloatArray3(cl)
    return func(a, b, c, d, e)

def listToCFloatArray3(inp):
    out = float_vector3()
    out[0] = inp[0]
    out[1] = inp[1]
    out[2] = inp[2]
    return out

def listToCFloatArray2(inp):
    out = float_vector2()
    out[0] = inp[0]
    out[1] = inp[1]
    return out

def listToCFloatArrayX(inp):
    out = float_vector_X()
    for i in range(NUMX):
        out[i] = inp[i]
    return out

def listToCFloatArray4(inp):
    out = float_vector4()
    out[0] = inp[0]
    out[1] = inp[1]
    out[2] = inp[2]
    out[3] = inp[3]
    return out


# API functions

INSGPSInit = getFunction("INSGPSInit")
INSStatePrediction = getFunction("INSStatePrediction",
                                 [ float_vector3, float_vector3, c_float ])
INSCovariancePrediction = getFunction("INSCovariancePrediction", [ c_float ])
INSCorrection   = getFunction("INSCorrection",
                              [ float_vector3, float_vector3, float_vector3,
                                c_float, c_uint16 ])
INSResetP       = getFunction("INSResetP", [ float_vector_X ])
INSGetP         = getFunction("INSGetP",   [ float_vector_X ])
INSSetState     = getFunction("INSSetState", [ float_vector3, float_vector3,
                                               float_vector4, float_vector3,
                                               float_vector3 ])
INSSetPosVelVar = getFunction("INSSetPosVelVar", [ float_vector3, float_vector3 ])
INSSetGyroBias  = getFunction("INSSetGyroBias", [ float_vector3 ])
INSSetAccelVar  = getFunction("INSSetAccelVar", [ float_vector3 ])
INSSetGyroVar   = getFunction("INSSetGyroVar",  [ float_vector3 ])
INSSetGyroBiasVar = getFunction("INSSetGyroBiasVar", [ float_vector3 ])
INSSetMagNorth  = getFunction("INSSetMagNorth", [ float_vector3 ])
INSSetMagVar    = getFunction("INSSetMagVar",   [ float_vector3 ])
INSPosVelReset  = getFunction("INSPosVelReset", [ float_vector3, float_vector3 ])

MagCorrection   = getFunction("MagCorrection",  [ float_vector3 ])
MagVelBaroCorrection = getFunction("MagVelBaroCorrection",
                                   [ float_vector3, float_vector3, c_float ])
FullCorrection  = getFunction("FullCorrection",
                        [ float_vector3, float_vector3, float_vector3, c_float ])
GpsBaroCorrection = getFunction("GpsBaroCorrection",
                                [ float_vector3, float_vector3, c_float ])
GpsMagCorrection  = getFunction("GpsMagCorrection",
                                [ float_vector3, float_vector3, float_vector2 ])
VelBaroCorrection = getFunction("VelBaroCorrection", [ float_vector3, c_float ])

ins_get_num_states = getFunction("ins_get_num_states", None, c_uint16)


# expose some non private functions
INSCorrection   = getFunction("INSCorrection",[ float_vector3, float_vector3,
                                                float_vector3, c_float, c_uint16 ])


INSStatePredictionL   = partial(convertFunction1, INSStatePrediction)
INSCorrectionL        = partial(convertFunction4, INSCorrection)
INSResetPL            = partial(convertFunction7, INSResetP)

zeroes = [0.0] * 3

GPS_SENSORS = HORIZ_SENSORS + VERT_SENSORS + POS_SENSORS

def GpsCorrection(pos, vel):
    return INSCorrectionL( zeroes, pos, vel, 0.0, GPS_SENSORS )

def INSGetPL():
    temp = float_vector_X()
    INSGetP(byref(temp))
    out = [float(x) for x in temp ]
    return out

INSSetStateL          = partial(convertFunction8, INSSetState)
INSetPosVelVarL       = partial(convertFunction3, INSSetPosVelVar)
INSSetGyroBiasL       = partial(convertFunction2, INSSetGyroBias)
INSSetAccelVarL       = partial(convertFunction2, INSSetAccelVar)
INSSetGyroVarL        = partial(convertFunction2, INSSetGyroVar)
INSSetGyroBiasVarL    = partial(convertFunction2, INSSetGyroBiasVar)
INSSetMagNorthL       = partial(convertFunction2, INSSetMagNorth)
INSSetMagVarL         = partial(convertFunction2, INSSetMagVar)
INSPosVelResetL       = partial(convertFunction3, INSPosVelReset)

MagCorrectionL        = partial(convertFunction2, MagCorrection)
MagVelBaroCorrectionL = partial(convertFunction1, MagVelBaroCorrection)
FullCorrectionL       = partial(convertFunction4, FullCorrection)
GpsBaroCorrectionL    = partial(convertFunction1, GpsBaroCorrection)
GpsMagCorrectionL     = partial(convertFunction5, GpsMagCorrection)
VelBaroCorrectionL    = partial(convertFunction6, VelBaroCorrection)
INSCorrectionL        = partial(convertFunction9, INSCorrection)



if __name__ == '__main__':
    INSGPSInit()
    INSStatePrediction(listToCFloatArray3([0.0, 0.0, 0.0]),
                       listToCFloatArray3([0.0, 0.0, 0.0]),
                       0.1)
    INSStatePredictionL([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 0.1)
    INSCovariancePrediction(0.1)

    for i in range(NUMX):
        for j in range(NUMX):
            print '%7.2f' % ekf.F[i][j],
        print

    print Nav
